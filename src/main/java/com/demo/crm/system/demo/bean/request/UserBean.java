package com.demo.crm.system.demo.bean.request;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class UserBean {
	
	/** The id. */
	private int id;

	/** The first name. */
	@NotBlank(message="Please enter first name.")
	@Size(max=64, message="Please enter length of first name should be less than or equals to 64.")
	private String firstName;

	/** The last name. */
	@Size(max=64, message="Please enter length of last name should be less than or equals to 64.")
	private String lastName;

	/** The user name. */
	@Size(min=5, message="Please enter minimum 5 characters for username.")
	@Size(max=20, message="Please enter length of user name should be less than or equals to 20.")
	private String userName;

	/** The mobile number. */
	@Pattern(regexp="^(\\+\\d{1,3}[- ]?)?\\d{10}$", message="Please enter valid mobile number.")
	private String mobileNumber;

	/** The email address. */
	@NotBlank(message="Please enter email address.")
	@Email(message="Please enter valid email address.")
	@Size(max=64, message="Please enter length of email name should be less than or equals to 64.")
	private String emailAddress;

	/** The password. */
	@Size(min=8, message="Please enter minimum 8 characters for password.")
	@Size(max=15, message="Please enter minimum 15 digits for password.")
	@Pattern(regexp = "([a-zA-Z0-9.]{8,15}$)", message="Please enter only alphanumberic and . for password.")
	private String password;

	/** The birth date. */
	@Pattern(regexp = "^(3[01]|[12][0-9]|0[1-9])/(1[0-2]|0[1-9])/[0-9]{4}$", message="Please enter birthDate in format of dd/MM/yyyy.")
	private String birthDate;

	/** The address. */
	@Size(max=2000, message="Please enter address length should be less than or equals to 64.")
	private String address;
}