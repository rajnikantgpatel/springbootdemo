package com.demo.crm.system.demo.controller;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.crm.system.demo.bean.request.UserBean;
import com.demo.crm.system.demo.bean.response.AbstractResponseBean;
import com.demo.crm.system.demo.service.IUserService;

/**
 * The Class UserController.
 * 
 * @author Rajnikant Patel
 *
 */
/**
 * @author Rajnikant Patel
 *
 */
@RestController
@RequestMapping("/user")
public class UserController extends AbstractController {

	/** The user service. */
	@Autowired
	private IUserService userService;

	/**
	 * Adds the user.
	 *
	 * @param user
	 *            the user
	 * @return the response entity
	 */
	@PostMapping(value = "/add", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AbstractResponseBean> addUser(@Valid @RequestBody UserBean user) {
		UserBean bean = userService.createOrUpdate(user);
		if (null != bean) {
			return buildResponse(bean, 0, HttpStatus.CREATED, new Date());
		} else {
			return buildResponse("Internal Server Error", 0, HttpStatus.OK, new Date());
		}
	}

	/**
	 * Gets the user by id.
	 *
	 * @param id
	 *            the id
	 * @return the user by id
	 */
	@GetMapping(value = "/get/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AbstractResponseBean> getUserById(@Valid @PathVariable("id") int id) {
		UserBean bean = userService.getById(id);
		if (null != bean) {
			return buildResponse(bean, 0, HttpStatus.OK, new Date());
		} else {
			return buildResponse(bean, 0, HttpStatus.OK, new Date());
		}

	}

	/**
	 * Find all.
	 *
	 * @return the response entity
	 */
	@GetMapping(value = "/list", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AbstractResponseBean> findAll() {
		List<UserBean> userList = userService.findAll();
		return buildResponse(userList, userList.size(), HttpStatus.OK, new Date());
	}

	/**
	 * Delete user.
	 *
	 * @param id
	 *            the id
	 * @return the response entity
	 */
	@DeleteMapping(value = "/delete/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AbstractResponseBean> deleteUser(@Valid @PathVariable("id") int id) {
		boolean isDeleted = userService.delete(id);
		return buildResponse("", "", "User was" + ((isDeleted) ? "" : " not") + " deleted successfully", ((isDeleted) ? HttpStatus.OK : HttpStatus.NOT_FOUND),
				new Date());
	}

	/**
	 * Test user controller.
	 *
	 * @return the response entity
	 */
	@GetMapping(value = "/test", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AbstractResponseBean> testUserController() {
		return buildResponse("Rajnikant's first User Controller test.", 0, HttpStatus.OK, new Date());
	}
}
