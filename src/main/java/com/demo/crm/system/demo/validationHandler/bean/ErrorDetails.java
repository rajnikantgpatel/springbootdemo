package com.demo.crm.system.demo.validationHandler.bean;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The Class ErrorDetails.
 * 
 * @author Rajnikant Patel
 *
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ErrorDetails {

	/** The timestamp. */
	private Date timestamp;

	/** The message. */
	private String message;

	/** The details. */
	private String details;
}