package com.demo.crm.system.demo.bean.response;


import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AbstractResponseBean<ID ,T> extends AbstractResponse {

    private T data;
    private ID objectId;

    private Integer totalResultsCount;
    private Integer resultCountPerPage;
    private Integer currentPageIndex;

}
