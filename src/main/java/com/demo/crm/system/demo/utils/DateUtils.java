package com.demo.crm.system.demo.utils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.springframework.util.StringUtils;

/**
 * The Class DateUtils.
 * 
 * @author Rajnikant Patel
 *
 */
public class DateUtils {

	/**
	 * Gets the date from string.
	 *
	 * @param strDate
	 *            the str date
	 * @return the date from string
	 */
	public static LocalDate getDateFromString(String strDate) {
		LocalDate localDate = null;
		if (!StringUtils.isEmpty(strDate)) {
			DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
			localDate = LocalDate.parse(strDate, dateTimeFormatter);
		}
		return localDate;
	}

	/**
	 * Gets the string from date.
	 *
	 * @param date
	 *            the date
	 * @return the string from date
	 */
	public static String getStringFromDate(LocalDate date) {
		if (null != date) {
			DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
			return date.format(dateTimeFormatter);
		}
		return null;
	}

}
