package com.demo.crm.system.demo.bean.response;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class AbstractResponse {

    /** The authenticated. */
    private Boolean authenticated = false;
    
    /** The success. */
    private Boolean success = false;
    
    /** The message. */
    private String message;

    /** The error messages. */
    private List<String> errorMessages = new ArrayList<String>();
    
    /** The timestamp. */
    private Date timestamp;

    /**
	 * Instantiates a new abstract response.
	 *
	 * @param authenticated
	 *            the authenticated
	 * @param success
	 *            the success
	 * @param errormap
	 *            the errormap
	 */
    public AbstractResponse(Boolean authenticated, Boolean success, List<String> errormap) {
        this.authenticated = authenticated;
        this.success = success;
        this.errorMessages = errormap;
    }

    /**
	 * Instantiates a new abstract response.
	 *
	 * @param authenticated
	 *            the authenticated
	 * @param success
	 *            the success
	 * @param message
	 *            the message
	 */
    public AbstractResponse(Boolean authenticated, Boolean success, String message) {
        this.authenticated = authenticated;
        this.success = success;
        this.message = message;
    }

    /**
	 * Instantiates a new abstract response.
	 *
	 * @param authenticated
	 *            the authenticated
	 * @param success
	 *            the success
	 * @param data
	 *            the data
	 */
    public AbstractResponse(Boolean authenticated, Boolean success, Object data) {
        this.authenticated = authenticated;
        this.success = success;
    }

    /**
	 * Instantiates a new abstract response.
	 *
	 * @param authenticated
	 *            the authenticated
	 * @param success
	 *            the success
	 * @param message
	 *            the message
	 * @param data
	 *            the data
	 */
    public AbstractResponse(Boolean authenticated, Boolean success, String message, Object data) {
        this.authenticated = authenticated;
        this.success = success;
        this.message = message;

    }

    /**
	 * Instantiates a new abstract response.
	 *
	 * @param authenticated
	 *            the authenticated
	 * @param success
	 *            the success
	 */
    public AbstractResponse(Boolean authenticated, Boolean success) {
        this.authenticated = authenticated;
        this.success = success;
    }
}
