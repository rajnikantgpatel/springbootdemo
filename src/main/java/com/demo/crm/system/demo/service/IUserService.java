package com.demo.crm.system.demo.service;

import java.util.List;

import com.demo.crm.system.demo.bean.request.UserBean;

/**
 * The Interface IUserService.
 * 
 * @author Rajnikant Patel
 *
 */
public interface IUserService {

	/**
	 * Creates the or update.
	 *
	 * @param user
	 *            the user
	 * @return the user bean
	 */
	UserBean createOrUpdate(UserBean user);

	/**
	 * Gets the by id.
	 *
	 * @param id
	 *            the id
	 * @return the by id
	 */
	UserBean getById(Integer id);

	/**
	 * Find all users.
	 *
	 * @return the list
	 */
	List<UserBean> findAll();

	/**
	 * Delete user.
	 *
	 * @param id
	 *            the id
	 * @return true, if successful
	 */
	boolean delete(Integer id);
}
