package com.demo.crm.system.demo.converter;


import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.demo.crm.system.demo.bean.request.UserBean;
import com.demo.crm.system.demo.entity.UserEntity;
import com.demo.crm.system.demo.utils.DateUtils;

/**
 * The Class UserConverter.
 * 
 * @author Rajnikant Patel
 *
 */
@Component
public class UserConverter {

    /**
	 * Populate user entity to bean.
	 *
	 * @param bean
	 *            the bean
	 * @param entity
	 *            the entity
	 */
    @SuppressWarnings("Duplicates")
    public void populateUserEntityToBean(UserBean bean, UserEntity entity){
        if(null != entity){
        	getUserEntityToBean(bean, entity);
        }
    }


    /**
	 * Populate user bean to entity.
	 *
	 * @param entity
	 *            the entity
	 * @param bean
	 *            the bean
	 */
    @SuppressWarnings("Duplicates")
    public void populateUserBeanToEntity(UserEntity entity,UserBean bean){
        if(null != bean){
        	getUserBeanToEntity(entity, bean);
        }
    }

    /**
	 * Gets the user entity from bean.
	 *
	 * @param bean
	 *            the bean
	 * @return the user entity from bean
	 */
    @SuppressWarnings("Duplicates")
    public UserEntity getUserEntityFromBean(UserBean bean) {
    	UserEntity entity = new UserEntity();
        if(null != bean){
        	getUserBeanToEntity(entity, bean);
        }
        return entity;
    }

    /**
	 * Gets the user bean from entity.
	 *
	 * @param entity
	 *            the entity
	 * @return the user bean from entity
	 */
    @SuppressWarnings("Duplicates")
    public UserBean getUserBeanFromEntity(UserEntity entity) {
    	UserBean bean = new UserBean();
        if(null != entity){
        	getUserEntityToBean(bean, entity);
        }
        return bean;
    }

    /**
	 * Gets the user bean list from entity.
	 *
	 * @param userEntityList
	 *            the user entity list
	 * @return the user bean list from entity
	 */
    public List<UserBean> getUserBeanListFromEntity(List<UserEntity> userEntityList) {
        List<UserBean> beans = new ArrayList<>();
        for(UserEntity entity :userEntityList){
            beans.add(getUserBeanFromEntity(entity));
        }
        return beans;
    }

    /**
	 * Gets the user entity to bean.
	 *
	 * @param bean
	 *            the bean
	 * @param entity
	 *            the entity
	 * @return the user entity to bean
	 */
    private UserBean getUserEntityToBean(UserBean bean, UserEntity entity){
        if(null != entity){
            bean.setId(entity.getId());
            bean.setFirstName(entity.getFirstName());
            bean.setLastName(entity.getLastName());
            bean.setUserName(entity.getUserName());
            bean.setMobileNumber(entity.getMobileNumber());
            bean.setEmailAddress(entity.getEmailAddress());
            bean.setPassword(entity.getPassword());
            bean.setBirthDate(DateUtils.getStringFromDate(entity.getBirthDate()));
            bean.setAddress(entity.getAddress());
        }
        return bean;
    }

    /**
	 * Gets the user bean to entity.
	 *
	 * @param entity
	 *            the entity
	 * @param bean
	 *            the bean
	 * @return the user bean to entity
	 */
    public void getUserBeanToEntity(UserEntity entity,UserBean bean){
        if(null != bean){
        	entity.setId(bean.getId());
        	entity.setFirstName(bean.getFirstName());
        	entity.setLastName(bean.getLastName());
        	entity.setUserName(bean.getUserName());
        	entity.setMobileNumber(bean.getMobileNumber());
        	entity.setEmailAddress(bean.getEmailAddress());
        	entity.setPassword(bean.getPassword());
        	entity.setBirthDate( DateUtils.getDateFromString(bean.getBirthDate()));
        	entity.setAddress(bean.getAddress());
        }
    }
}
