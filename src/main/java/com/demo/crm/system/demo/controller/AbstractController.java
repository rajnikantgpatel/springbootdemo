package com.demo.crm.system.demo.controller;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.demo.crm.system.demo.bean.response.AbstractResponseBean;

/**
 * The Class AbstractController.
 * 
 * @author Rajnikant Patel
 *
 */
public class AbstractController {

	/** The valid sort order. */
	private List<String> validSortOrder = Arrays.asList("ASC", "DESC");

	/*
	 * public ResponseEntity<AbstractResponse> buildResponse(Object data, HttpStatus
	 * status) { AbstractResponse response = new AbstractResponse(true, true, data);
	 * return new ResponseEntity(response, status); }
	 */

	/**
	 * Builds the response.
	 *
	 * @param data
	 *            the data
	 * @param resultCount
	 *            the result count
	 * @param status
	 *            the status
	 * @param timeStamp
	 *            the time stamp
	 * @return the response entity
	 */
	public ResponseEntity<AbstractResponseBean> buildResponse(Object data, Integer resultCount, HttpStatus status,
			Date timeStamp) {
		AbstractResponseBean<Long, Object> responseBean = new AbstractResponseBean<Long, Object>();
		responseBean.setTotalResultsCount(resultCount);
		responseBean.setData(data);
		responseBean.setTimestamp(timeStamp);
		if (HttpStatus.OK == status || HttpStatus.CREATED == status)
			responseBean.setSuccess(true);
		return new ResponseEntity<>(responseBean, status);

	}

	/**
	 * Builds the response.
	 *
	 * @param data
	 *            the data
	 * @param ID
	 *            the id
	 * @param status
	 *            the status
	 * @param timeStamp
	 *            the time stamp
	 * @return the response entity
	 */
	public ResponseEntity<AbstractResponseBean> buildResponse(Object data, String ID, HttpStatus status,
			Date timeStamp) {
		AbstractResponseBean<String, Object> responseBean = new AbstractResponseBean<String, Object>();
		responseBean.setObjectId(ID);
		responseBean.setData(data);
		responseBean.setTimestamp(timeStamp);
		if (HttpStatus.OK == status)
			responseBean.setSuccess(true);
		return new ResponseEntity<>(responseBean, status);
	}

	/**
	 * Builds the response.
	 *
	 * @param data
	 *            the data
	 * @param ID
	 *            the id
	 * @param message
	 *            the message
	 * @param status
	 *            the status
	 * @param timeStamp
	 *            the time stamp
	 * @return the response entity
	 */
	public ResponseEntity<AbstractResponseBean> buildResponse(Object data, String ID, String message, HttpStatus status,
			Date timeStamp) {
		AbstractResponseBean<String, Object> responseBean = new AbstractResponseBean<String, Object>();
		responseBean.setObjectId(ID);
		responseBean.setData(data);
		responseBean.setMessage(message);
		responseBean.setTimestamp(timeStamp);
		if (HttpStatus.OK == status)
			responseBean.setSuccess(true);
		return new ResponseEntity<>(responseBean, status);
	}
}
