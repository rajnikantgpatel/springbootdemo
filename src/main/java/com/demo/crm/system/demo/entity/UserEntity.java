package com.demo.crm.system.demo.entity;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "user_master")
@NoArgsConstructor
public class UserEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false, length = 64)
	private int id;

	@Column(name = "first_name", nullable = false, length = 64)
	private String firstName;

	@Column(name = "last_name", length = 64)
	private String lastName;

	@Column(name = "user_name", nullable = false, length = 15, unique = true)
	private String userName;

	@Column(name = "mobile_number", nullable = false, length = 15)
	private String mobileNumber;

	@Column(name = "email_address", nullable = false, length = 64, unique = true)
	private String emailAddress;

	@Column(name = "password", nullable = false, length = 15)
	private String password;

	@Column(name = "birth_date", nullable = false)
	private LocalDate birthDate;

	@Column(name = "address", length = 2000)
	private String address;
}
