package com.demo.crm.system.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * The Class DemoApplication.
 * 
 * @author Rajnikant Patel
 *
 */
@SpringBootApplication
@EnableAutoConfiguration
public class DemoApplication {

	/**
	 * The main method.
	 *
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

}
