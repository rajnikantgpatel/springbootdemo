package com.demo.crm.system.demo.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.crm.system.demo.bean.request.UserBean;
import com.demo.crm.system.demo.converter.UserConverter;
import com.demo.crm.system.demo.entity.UserEntity;
import com.demo.crm.system.demo.repository.IUserRepository;
import com.demo.crm.system.demo.service.IUserService;

/**
 * The Class UserServiceImpl.
 * 
 * @author Rajnikant Patel
 *
 */
@Service
public class UserServiceImpl implements IUserService {

	/** The user repository. */
	@Autowired
	IUserRepository userRepository;

	/** The user converter. */
	@Autowired
	UserConverter userConverter;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.demo.crm.system.demo.service.IUserService#createOrUpdateUser(com.demo.crm
	 * .system.demo.bean.request.UserBean)
	 */
	@Override
	public UserBean createOrUpdate(UserBean userBean) {
		UserEntity entity = userConverter.getUserEntityFromBean(userBean);
		UserEntity newEntity = userRepository.save(entity);
		return userConverter.getUserBeanFromEntity(newEntity);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.demo.crm.system.demo.service.IUserService#getUserById(java.lang.Long)
	 */
	@Override
	public UserBean getById(Integer id) {
		Optional<UserEntity> optionalUser = userRepository.findById(id);
		if (optionalUser.isPresent()) {
			return userConverter.getUserBeanFromEntity(optionalUser.get());
		} else {
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.demo.crm.system.demo.service.IUserService#findAll()
	 */
	@Override
	public List<UserBean> findAll() {
		return userConverter.getUserBeanListFromEntity(userRepository.findAll());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.demo.crm.system.demo.service.IUserService#deleteUser(java.lang.Long)
	 */
	@Override
	public boolean delete(Integer id) {
		Optional<UserEntity> optionalUser = userRepository.findById(id);
		if (optionalUser.isPresent()) {
			userRepository.delete(optionalUser.get());
			return true;
		} else {
			return false;
		}
	}
}
