package com.demo.crm.system.demo.validationHandler.handlerAdvice;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.demo.crm.system.demo.bean.response.AbstractResponseBean;

/**
 * The Class CustomizedResponseEntityExceptionHandler.
 * 
 * @author Rajnikant Patel
 *
 */
@ControllerAdvice
@RestController
public class CustomizedResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.web.servlet.mvc.method.annotation.
	 * ResponseEntityExceptionHandler#handleMethodArgumentNotValid(org.
	 * springframework.web.bind.MethodArgumentNotValidException,
	 * org.springframework.http.HttpHeaders, org.springframework.http.HttpStatus,
	 * org.springframework.web.context.request.WebRequest)
	 */
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		AbstractResponseBean<Long, Object> responseBean = new AbstractResponseBean<Long, Object>();
		responseBean.setSuccess(false);
		responseBean.setTimestamp(new Date());
		if (ex.getBindingResult() != null && ex.getBindingResult().hasErrors()) {

			List<String> errorMessages = new ArrayList<String>();
			if (!CollectionUtils.isEmpty(ex.getBindingResult().getAllErrors())) {
				for (ObjectError validationError : ex.getBindingResult().getAllErrors()) {
					if (!StringUtils.isEmpty(validationError.getDefaultMessage())) {
						errorMessages.add(validationError.getDefaultMessage());
					}
				}
			}
			responseBean.setErrorMessages(errorMessages);
			responseBean.setMessage("Validation failed");
		} else {
			responseBean.setMessage("Unknown error occured");
		}
		return new ResponseEntity<>(responseBean, HttpStatus.BAD_REQUEST);
	}
}
  